# standard imports
import logging

# external imports
from chainlib.eth.error import (
        EthException,
        RevertEthException,
        )

logg = logging.getLogger().getChild(__name__)


class CICRegistryException(Exception):
    pass


class UnknownContractError(CICRegistryException):
    pass


class NotAContractError(CICRegistryException):
    pass


class ContractMismatchError(CICRegistryException):
    pass


class LookupErrorParser:

    @classmethod
    def translate(self, error):

        if error['error']['code'] == -32015:
            raise RevertEthException('{} {}'.format(error['error']['message'], error['error']['data']))

        raise EthException(error)
