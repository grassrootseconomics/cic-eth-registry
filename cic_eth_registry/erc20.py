# standard imports
import logging

# external imports
from eth_erc20 import ERC20
from chainlib.connection import RPCConnection
from chainlib.eth.contract import code
from chainlib.eth.error import RevertEthException
from hexathon import (
        strip_0x,
        add_0x,
        )

# local imports
from cic_eth_registry.cache.base import CacheObject
from cic_eth_registry.base import QueryBase
from cic_eth_registry.error import (
        NotAContractError,
        LookupErrorParser,
        ContractMismatchError,
        )

logg = logging.getLogger().getChild(__name__)


class ERC20TokenCacheObject(CacheObject):

    def __init__(self, chain_spec, address):
        super(ERC20TokenCacheObject, self).__init__(chain_spec, 'erc20', address, ['name', 'symbol', 'decimals', 'supply'])


class ERC20Token(QueryBase):

    def __init__(self, chain_spec, rpc, address):
        self.o = ERC20TokenCacheObject(chain_spec, address)

        self.address = self.o.address

        if self.o.cache_date == None:
            o = code(add_0x(address))
            r = rpc.do(o)
            if len(strip_0x(r, allow_empty=True)) == 0:
                raise NotAContractError(address)
            
            try:
                self.load(rpc)
            except RevertEthException:
                raise ContractMismatchError(address)



    def load(self, rpc):
        c = ERC20(self.o.chain_spec)
        o = c.name(self.o.address, sender_address=self.caller_address)
        r = rpc.do(o, error_parser=LookupErrorParser)
        self.o.name = c.parse_name(r)

        o = c.symbol(self.o.address, sender_address=self.caller_address)
        r = rpc.do(o, error_parser=LookupErrorParser)
        self.o.symbol = c.parse_symbol(r)

        o = c.decimals(self.o.address, sender_address=self.caller_address)
        r = rpc.do(o, error_parser=LookupErrorParser)
        self.o.decimals = c.parse_decimals(r)

        o = c.decimals(self.o.address, sender_address=self.caller_address)
        r = rpc.do(o, error_parser=LookupErrorParser)
        self.o.decimals = c.parse_decimals(r)

        o = c.total_supply(self.o.address, sender_address=self.caller_address)
        r = rpc.do(o, error_parser=LookupErrorParser)
        self.o.supply = c.parse_total_supply(r)

        logg.debug('loaded: {}'.format(self))


    @property
    def name(self):
        return self.o.name


    @property
    def symbol(self):
        return self.o.symbol


    @property
    def decimals(self):
        return self.o.decimals


    @property
    def supply(self):
        return self.o.supply


    def __str__(self):
        return 'ERC20 {} ({}) decimals {} supply {} address {}'.format(
            self.name,
            self.symbol,
            self.decimals,
            self.supply,
            self.address,
                )
