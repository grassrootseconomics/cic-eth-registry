# standard imports
import os
import logging

# external imports
import pytest
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.tx import receipt
from giftable_erc20_token import GiftableToken
from hexathon import add_0x
from chainlib.eth.pytest.fixtures_ethtester import *

# local imports
from cic_eth_registry.erc20 import ERC20Token
from cic_eth_registry.pytest.fixtures_contracts import *

logg = logging.getLogger()


@pytest.fixture(scope='function')
def token_roles(
        eth_accounts,
        contract_roles,
        ):

    more_roles = {
        'FOO_TOKEN_OWNER': eth_accounts[10],
        'BAR_TOKEN_OWNER': eth_accounts[11],
            }
    ERC20Token.caller_address = contract_roles['DEFAULT']
    return more_roles


@pytest.fixture(scope='function')
def foo_token(
        default_chain_spec,
        eth_rpc,
        eth_signer,
        eth_accounts,
        token_roles,
        ):

    nonce_oracle = RPCNonceOracle(token_roles['FOO_TOKEN_OWNER'], eth_rpc)
    c = GiftableToken(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.constructor(token_roles['FOO_TOKEN_OWNER'], 'Foo Token', 'FOO', 6)
    r = eth_rpc.do(o)
    assert tx_hash_hex == r

    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)

    token_address = r['contract_address']
    logg.debug('deployed foo token at address {}'.format(token_address))

    (tx_hash_hex, o) = c.mint_to(token_address, token_roles['FOO_TOKEN_OWNER'], token_roles['FOO_TOKEN_OWNER'], (10 ** 9) * (10 ** 6))
    eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1 

    return token_address


@pytest.fixture(scope='function')
def foo_token_declaration(
        foo_token,
        ):
    return add_0x(os.urandom(32).hex())


@pytest.fixture(scope='function')
def bar_token(
        default_chain_spec,
        eth_rpc,
        eth_signer,
        eth_accounts,
        token_roles,
        ):

    nonce_oracle = RPCNonceOracle(token_roles['BAR_TOKEN_OWNER'], eth_rpc)
    c = GiftableToken(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.constructor(token_roles['BAR_TOKEN_OWNER'], 'Bar Token', 'BAR', 9)
    r = eth_rpc.do(o)
    assert tx_hash_hex == r

    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)

    token_address = r['contract_address']
    logg.debug('deployed bar token at address {}'.format(token_address))

    (tx_hash_hex, o) = c.mint_to(token_address, token_roles['BAR_TOKEN_OWNER'], token_roles['BAR_TOKEN_OWNER'], (10 ** 9) * (10 ** 9))
    eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1 

    return token_address


@pytest.fixture(scope='function')
def bar_token_declaration(
        bar_token,
        ):
    return add_0x(os.urandom(32).hex())
