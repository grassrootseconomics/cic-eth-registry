# standard imports
import logging
import json
import hashlib

# external imports
import pytest
from eth_accounts_index.registry import AccountRegistry
from okota.token_index.index import TokenUniqueSymbolIndexAddressDeclarator as TokenUniqueSymbolIndex
from eth_address_declarator.declarator import AddressDeclarator
from erc20_faucet.faucet import SingleShotFaucet 
from erc20_faucet import Faucet
from giftable_erc20_token import GiftableToken
from erc20_transfer_authorization import TransferAuthorization
from chainlib.eth.nonce import RPCNonceOracle
from hexathon import add_0x
from chainlib.eth.tx import receipt
from eth_contract_registry.registry import ContractRegistry
from eth_contract_registry.encoding import to_identifier
from eth_contract_registry.pytest.fixtures_registry import valid_identifiers

# local imports
from cic_eth_registry.registry import CICRegistry
from cic_eth_registry.base import QueryBase
from cic_eth_registry.lookup.tokenindex import TokenIndexLookup
from cic_eth_registry.lookup.declarator import AddressDeclaratorLookup
from eth_contract_registry.pytest.fixtures_registry import *

#logg = logging.getLogger(__name__)
logg = logging.getLogger(__name__)

valid_identifiers += [
        'AccountRegistry',
        'TokenRegistry',
        'AddressDeclarator',
        'Faucet',
        'TransferAuthorization',
        ]


@pytest.fixture(scope='function')
def contract_roles(
        roles,
        eth_accounts,
        ):


    more_roles = {}
    more_roles.update(roles)
    more_roles.update({
        'ACCOUNT_REGISTRY_WRITER': eth_accounts[2],
        'TRUSTED_DECLARATOR': eth_accounts[3],
        'FAUCET_WRITER': eth_accounts[4],
            })
    QueryBase.caller_address = roles['DEFAULT']
    return more_roles


@pytest.fixture(scope='function')
def cic_registry(
        default_chain_spec,
        eth_rpc,
        registry,
        ):

    CICRegistry.address = registry

    return registry


@pytest.fixture(scope='function')
def purge_lookups():
    CICRegistry.lookups = []
    CICRegistry.lookups_reverse = []


# TODO: missing lookup registration when using the cic_registry fixture can lead to confusing errors downstream. A better solution may be a single fixture that makes sure the lookups are registered.
@pytest.fixture(scope='function')
def register_lookups(
        default_chain_spec,
        eth_rpc,
        cic_registry,
        address_declarator,
        purge_lookups,
        token_registry,
        contract_roles,
        ):

    lookup = TokenIndexLookup(default_chain_spec, token_registry)
    CICRegistry.add_lookup(lookup)

    lookup_reverse = AddressDeclaratorLookup(default_chain_spec, address_declarator, [contract_roles['TRUSTED_DECLARATOR']])
    CICRegistry.add_lookup(lookup_reverse)

    return cic_registry


@pytest.fixture(scope='function')
def account_registry(
        registry,
        eth_signer,
        eth_rpc,
        default_chain_spec,
        default_chain_config,
        contract_roles,
        ):

    nonce_oracle = RPCNonceOracle(contract_roles['CONTRACT_DEPLOYER'], eth_rpc)

    c = AccountRegistry(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.constructor(contract_roles['CONTRACT_DEPLOYER'])
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1
    account_registry_address = r['contract_address']

    (tx_hash_hex, o) = c.add_writer(account_registry_address, contract_roles['CONTRACT_DEPLOYER'], contract_roles['ACCOUNT_REGISTRY_WRITER'])
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    c = ContractRegistry(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)

    chain_spec_identifier = to_identifier(str(default_chain_spec))

    h = hashlib.new('sha256')
    j = json.dumps(default_chain_config)
    h.update(j.encode('utf-8'))
    z = h.digest()
    chain_config_digest = add_0x(z.hex())

    (tx_hash_hex, o) = c.set(registry, contract_roles['CONTRACT_DEPLOYER'], 'AccountRegistry', account_registry_address)
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    logg.info('accounts registry deployed: {}'.format(account_registry_address))
    return account_registry_address


@pytest.fixture(scope='function')
def token_registry(
        registry,
        eth_signer,
        eth_rpc,
        default_chain_spec,
        default_chain_config,
        address_declarator,
        contract_roles,
        ):

    nonce_oracle = RPCNonceOracle(contract_roles['CONTRACT_DEPLOYER'], eth_rpc)

    c = TokenUniqueSymbolIndex(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.constructor(contract_roles['CONTRACT_DEPLOYER'], address_declarator)
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1
    token_registry_address = r['contract_address']

    (tx_hash_hex, o) = c.add_writer(token_registry_address, contract_roles['CONTRACT_DEPLOYER'], contract_roles['CONTRACT_DEPLOYER'])
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    c = ContractRegistry(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)

    chain_spec_identifier = to_identifier(str(default_chain_spec))

    h = hashlib.new('sha256')
    j = json.dumps(default_chain_config)
    h.update(j.encode('utf-8'))
    z = h.digest()
    chain_config_digest = add_0x(z.hex())

    (tx_hash_hex, o) = c.set(registry, contract_roles['CONTRACT_DEPLOYER'], 'TokenRegistry', token_registry_address)
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    logg.info('token registry deployed: {}'.format(token_registry_address))

    #lookup = TokenIndexLookup(default_chain_spec, token_registry_address)
    #CICRegistry.add_lookup(lookup)

    return token_registry_address


@pytest.fixture(scope='function')
def register_tokens(
    default_chain_spec,
    foo_token,
    foo_token_declaration,
    bar_token,
    bar_token_declaration,
    token_registry,
    address_declarator,
    contract_roles,
    eth_rpc,
    eth_signer,
    ):


    tokens = [foo_token, bar_token]
    token_declarations = [foo_token_declaration, bar_token_declaration]
    for i, token in enumerate(tokens):
        nonce_oracle = RPCNonceOracle(contract_roles['CONTRACT_DEPLOYER'], eth_rpc)
        c = TokenUniqueSymbolIndex(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.add(token_registry, contract_roles['CONTRACT_DEPLOYER'], token)
        eth_rpc.do(o)
        o = receipt(tx_hash_hex)
        r = eth_rpc.do(o)
        assert r['status'] == 1

        nonce_oracle = RPCNonceOracle(contract_roles['TRUSTED_DECLARATOR'], eth_rpc)
        c = AddressDeclarator(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.add_declaration(address_declarator, contract_roles['TRUSTED_DECLARATOR'], token, token_declarations[i])
        eth_rpc.do(o)
        o = receipt(tx_hash_hex)
        r = eth_rpc.do(o)
        assert r['status'] == 1

    return token_registry


@pytest.fixture(scope='function')
def initial_declarator_description():
    return to_identifier('Big boss')


@pytest.fixture(scope='function')
def address_declarator(
        registry,
        eth_signer,
        eth_rpc,
        default_chain_spec,
        default_chain_config,
        contract_roles,
        initial_declarator_description,
        ):

    nonce_oracle = RPCNonceOracle(contract_roles['CONTRACT_DEPLOYER'], eth_rpc)

    c = AddressDeclarator(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.constructor(contract_roles['CONTRACT_DEPLOYER'], initial_declarator_description)
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1
    address_declarator_address = r['contract_address']

    c = ContractRegistry(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)

    chain_spec_identifier = to_identifier(str(default_chain_spec))

    h = hashlib.new('sha256')
    j = json.dumps(default_chain_config)
    h.update(j.encode('utf-8'))
    z = h.digest()
    chain_config_digest = add_0x(z.hex())

    (tx_hash_hex, o) = c.set(registry, contract_roles['CONTRACT_DEPLOYER'], 'AddressDeclarator', address_declarator_address)
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    logg.info('address declarator deployed: {}'.format(address_declarator_address))

    return address_declarator_address


@pytest.fixture(scope='function')
def faucet_store(
        default_chain_spec,
        eth_signer,
        eth_rpc,
        contract_roles,
        ):

    nonce_oracle = RPCNonceOracle(contract_roles['CONTRACT_DEPLOYER'], eth_rpc)

    c = SingleShotFaucet(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.store_constructor(contract_roles['CONTRACT_DEPLOYER'])
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1
    faucet_store_address = r['contract_address']

    return faucet_store_address


def faucet_setup(
        faucet_store,
        registry,
        eth_signer,
        eth_rpc,
        default_chain_config,
        default_chain_spec,
        account_registry,
        foo_token,
        contract_roles,
        token_roles,
        ):

    nonce_oracle = RPCNonceOracle(contract_roles['CONTRACT_DEPLOYER'], eth_rpc)

    c = SingleShotFaucet(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.constructor(contract_roles['CONTRACT_DEPLOYER'], foo_token, faucet_store, account_registry, [contract_roles['FAUCET_WRITER']])
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1
    faucet_address = r['contract_address']

    c = ContractRegistry(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)

    chain_spec_identifier = to_identifier(str(default_chain_spec))

    h = hashlib.new('sha256')
    j = json.dumps(default_chain_config)
    h.update(j.encode('utf-8'))
    z = h.digest()
    chain_config_digest = add_0x(z.hex())

    (tx_hash_hex, o) = c.set(registry, contract_roles['CONTRACT_DEPLOYER'], 'Faucet', faucet_address)
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    nonce_oracle = RPCNonceOracle(token_roles['FOO_TOKEN_OWNER'], eth_rpc)
    c = GiftableToken(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.add_minter(foo_token, token_roles['FOO_TOKEN_OWNER'], faucet_address)
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    logg.info('faucet deployed: {}'.format(faucet_address))

    return faucet_address


@pytest.fixture(scope='function')
def faucet(
        faucet_store,
        registry,
        eth_signer,
        eth_rpc,
        default_chain_config,
        default_chain_spec,
        account_registry,
        foo_token,
        contract_roles,
        token_roles,
        ):
    return faucet_setup(
        faucet_store,
        registry,
        eth_signer,
        eth_rpc,
        default_chain_config,
        default_chain_spec,
        account_registry,
        foo_token,
        contract_roles,
        token_roles,
        )


@pytest.fixture(scope='function')
def faucet_noregistry(
        faucet_store,
        registry,
        eth_signer,
        eth_rpc,
        default_chain_config,
        default_chain_spec,
        foo_token,
        contract_roles,
        token_roles,
        ):
    return faucet_setup(
        faucet_store,
        registry,
        eth_signer,
        eth_rpc,
        default_chain_config,
        default_chain_spec,
        None,
        foo_token,
        contract_roles,
        token_roles,
        )



@pytest.fixture(scope='function')
def transfer_auth(
        default_chain_spec,
        default_chain_config,
        contract_roles,
        eth_rpc,
        eth_signer,
        registry,
        ):

    nonce_oracle = RPCNonceOracle(contract_roles['CONTRACT_DEPLOYER'], eth_rpc)

    c = TransferAuthorization(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.constructor(contract_roles['CONTRACT_DEPLOYER'])
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1
    transferauth_address = r['contract_address']

    c = ContractRegistry(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)

    chain_spec_identifier = to_identifier(str(default_chain_spec))

    h = hashlib.new('sha256')
    j = json.dumps(default_chain_config)
    h.update(j.encode('utf-8'))
    z = h.digest()
    chain_config_digest = add_0x(z.hex())

    (tx_hash_hex, o) = c.set(registry, contract_roles['CONTRACT_DEPLOYER'], 'TransferAuthorization', transferauth_address)
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    logg.info('transfer authorization contract deployed: {}'.format(transferauth_address))

    return transferauth_address

