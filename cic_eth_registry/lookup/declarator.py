# standard imports
import logging

# external imports
from chainlib.eth.constant import ZERO_ADDRESS
from chainlib.eth.address import is_checksum_address
from eth_address_declarator import Declarator

logg = logging.getLogger().getChild(__name__)


class AddressDeclaratorLookup:


    def __init__(self, chain_spec, declarator_contract_address, trusted_addresses):
        self.ifc = Declarator(chain_spec)
        self.contract_address = declarator_contract_address
        self.trusted_addresses = []
        for address in trusted_addresses:
            if not is_checksum_address(address):
                raise ValueError('{} is not a valid checksum address'.format(address))
            self.trusted_addresses.append(address)
        if len(self.trusted_addresses) == 0:
            raise ValueError('must define at least one trusted address')


    def by_address(self, rpc, subject_address, sender_address=ZERO_ADDRESS):
        for trusted_address in self.trusted_addresses:
            o = self.ifc.declaration(self.contract_address, trusted_address, subject_address, sender_address=sender_address)
            r = rpc.do(o)
            if r != None:
                try:
                    logg.warning('returning first declaration unchecked')
                    declaration = self.ifc.parse_declaration(r)
                    return declaration[0]
                except:
                    pass

        return None
