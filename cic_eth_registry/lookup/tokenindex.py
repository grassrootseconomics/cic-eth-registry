# external imports
from chainlib.eth.constant import ZERO_ADDRESS
from eth_token_index import TokenUniqueSymbolIndex


class TokenIndexLookup:

    def __init__(self, chain_spec, token_index_address):
        self.address = token_index_address
        self.ifc = TokenUniqueSymbolIndex(chain_spec)


    def by_name(self, rpc, token_symbol, sender_address=ZERO_ADDRESS):
        o = self.ifc.address_of(self.address, token_symbol, sender_address=sender_address)
        r = rpc.do(o)
        try:
            return self.ifc.parse_address_of(r)
        except ValueError:
            return None


    def __str__(self):
        return 'token index lookup'
