class Cache:

    def by_address(self, chain_spec, address):
        raise NotImplementedError


    def by_name(self, chain_spec, name):
        raise NotImplementedError


    def add(self, chain_spec, name, obj):
        raise NotImplementedError


class CacheObject:

    def __init__(self, chain_spec, typ, address, template):
        self.typ = typ
        self.address = address
        self.cache_date = None
        self.chain_spec = chain_spec
        for m in template:
            setattr(self, m, None)
