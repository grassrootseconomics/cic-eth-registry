# standard imports
import logging

# external imports
from eth_contract_registry.registry import Registry
from chainlib.eth.constant import ZERO_ADDRESS
from chainlib.eth.address import is_same_address
from hexathon import (
        strip_0x,
        add_0x,
        )

# local imports
from .error import UnknownContractError
logg = logging.getLogger(__name__)


def is_empty_response(v):
    return v == None or is_same_address(v, ZERO_ADDRESS) or strip_0x(v, allow_empty=True) == ''


class CICRegistry:

    address = None
    lookups = []
    lookups_reverse = []

    def __init__(self, chain_spec, rpc):
        self.chain_spec = chain_spec
        self.rpc = rpc


    def lookup(self, name, sender_address=ZERO_ADDRESS):
        c = Registry(self.chain_spec)
        o = c.address_of(self.address, name, sender_address=sender_address)
        try:
            r = self.rpc.do(o)
            address = c.parse_address_of(r)
            return address
        except ValueError:
            pass

        return ZERO_ADDRESS


    def lookup_reverse(self, address, sender_address=ZERO_ADDRESS):
        return None


    @staticmethod
    def add_lookup(lookup):
        if hasattr(lookup, 'by_name'):
            logg.info('Adding name lookup from {}'.format(str(lookup)))
            CICRegistry.lookups.append(lookup)
        if hasattr(lookup, 'by_address'):
            logg.info('Adding address lookup from {}'.format(str(lookup)))
            CICRegistry.lookups_reverse.append(lookup)


    def by_name(self, name, sender_address=ZERO_ADDRESS):
        for lookup in self.lookups:
            logg.debug('executing lookup {} for name {}'.format(str(lookup), name))
            address = lookup.by_name(self.rpc, name, sender_address)
            if not is_empty_response(address) and not is_same_address(address, ZERO_ADDRESS):
                logg.info('lookup {} found {} for name {}'.format(str(lookup), address, name))
                return address

        logg.debug('executing fallback lookup for name {}'.format(name))
        address = self.lookup(name, sender_address)
        if is_empty_response(address):
            raise UnknownContractError((str(self.chain_spec), name,))
        logg.info('fallback lookup found {} for name {}'.format(address, name))

        return address


    def by_address(self, address, sender_address=ZERO_ADDRESS):
        for lookup in self.lookups_reverse:
            logg.debug('executing reverse lookup {} for address {}'.format(str(lookup), address))
            r =lookup.by_address(self.rpc, address, sender_address)
            if not is_empty_response(r):
                logg.info('reverse lookup {} found {} for address {}'.format(str(lookup), r, address)) 
                return r

        r = self.lookup_reverse(address, sender_address)
        logg.debug('executing fallback reverse lookup for address {}'.format(address))
        if is_empty_response(r):
            raise UnknownContractError((str(self.chain_spec), address,))
        logg.info('reverse fallback lookup found {} for address {}'.format(r, address)) 

        return r


    @staticmethod
    def init(address, chain_spec, rpc, sender_address=ZERO_ADDRESS):
        if CICRegistry.address != None:
            raise AttributeError('already initialized as {}'.format(CICRegistry.address))

        CICRegistry.address = address
        registry = CICRegistry(chain_spec, rpc)
        verify_address = registry.by_name('ContractRegistry', sender_address=sender_address)
        if verify_address == ZERO_ADDRESS:
            CICRegistry.address = None
            raise ValueError('contract at address {} is not a contract registry'.format(address))
