# standard imports
import logging

# external imports
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.tx import receipt
from chainlib.eth.address import is_same_address
from eth_accounts_index import AccountsIndex
from eth_contract_registry.registry import (
        Registry,
        )

# local imports
from cic_eth_registry.registry import CICRegistry

logg = logging.getLogger()


def test_registry(
        default_chain_spec,
        eth_rpc,
        eth_signer,
        registry,
        account_registry,
        contract_roles,
        eth_empty_accounts,
        ):
 
    nonce_oracle = RPCNonceOracle(contract_roles['DEFAULT'], eth_rpc)
    c = Registry(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    o = c.address_of(registry, 'AccountRegistry', contract_roles['DEFAULT'])
    r = eth_rpc.do(o)
    r = c.parse_address_of(r)
    assert is_same_address(account_registry, r)

    nonce_oracle = RPCNonceOracle(contract_roles['ACCOUNT_REGISTRY_WRITER'], eth_rpc)
    c = AccountsIndex(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle)
    (tx_hash_hex, o) = c.add(account_registry, contract_roles['ACCOUNT_REGISTRY_WRITER'], eth_empty_accounts[0])
    r = eth_rpc.do(o)
    o = receipt(tx_hash_hex)
    r = eth_rpc.do(o)
    assert r['status'] == 1


def test_cic_registry(
        eth_rpc,
        default_chain_spec,
        contract_roles,
        cic_registry,
        ):

    c = CICRegistry(default_chain_spec, eth_rpc)
    r = c.by_name('ContractRegistry', sender_address=contract_roles['DEFAULT'])
    assert is_same_address(cic_registry, r)
