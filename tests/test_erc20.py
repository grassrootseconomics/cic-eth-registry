# local imports
from cic_eth_registry.erc20 import ERC20Token


def test_erc20(
        default_chain_spec,
        eth_rpc,
        cic_registry,
        foo_token,
        ):

    t = ERC20Token(default_chain_spec, eth_rpc, foo_token)
    assert t.name == 'Foo Token'
    assert t.symbol == 'FOO'
    assert t.decimals == 6
