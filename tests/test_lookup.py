# standard imports
import os

# external imports
import pytest
from chainlib.eth.constant import ZERO_ADDRESS
from chainlib.eth.address import is_same_address
from hexathon import (
        add_0x,
        strip_0x,
        )

# local imports
from cic_eth_registry import CICRegistry
from cic_eth_registry.error import UnknownContractError

bar_address = add_0x(os.urandom(20).hex())
baz_address = add_0x(os.urandom(20).hex())

class FooLookup:

    def by_name(self, rpc, name, sender_address=ZERO_ADDRESS):
        return ZERO_ADDRESS


class BarLookup:

    def by_name(self, rpc, name, sender_address=ZERO_ADDRESS):
        if name == 'bar':
            return bar_address
        return ZERO_ADDRESS


def test_lookup(
        default_chain_spec,
        eth_rpc,
        contract_roles,
        cic_registry,
        ):

    CICRegistry.add_lookup(FooLookup())
    CICRegistry.add_lookup(BarLookup())
    registry = CICRegistry(default_chain_spec, eth_rpc)

    with pytest.raises(UnknownContractError):
        registry.by_name('abcde', sender_address=contract_roles['DEFAULT'])

    bar_retrieved_address = registry.by_name('bar', sender_address=contract_roles['DEFAULT'])
    assert is_same_address(bar_retrieved_address, bar_address)

    cic_registry_retrieved_address = registry.by_name('ContractRegistry', sender_address=contract_roles['DEFAULT'])
    assert is_same_address(cic_registry_retrieved_address, cic_registry)


def test_token_lookup(
        default_chain_spec,
        eth_rpc,
        contract_roles,
        foo_token,
        foo_token_declaration,
        bar_token,
        bar_token_declaration,
        register_tokens,
        register_lookups,
        ):

    registry = CICRegistry(default_chain_spec, eth_rpc)
    address_from_symbol = registry.by_name('FOO', sender_address=contract_roles['DEFAULT']) 
    assert is_same_address(address_from_symbol, foo_token)

    declaration_from_address = registry.by_address(foo_token, sender_address=contract_roles['DEFAULT'])
    assert strip_0x(declaration_from_address) == strip_0x(foo_token_declaration)

    address_from_symbol = registry.by_name('BAR', sender_address=contract_roles['DEFAULT']) 
    assert is_same_address(address_from_symbol, bar_token)

    declaration_from_address = registry.by_address(bar_token, sender_address=contract_roles['DEFAULT'])
    assert strip_0x(declaration_from_address) == strip_0x(bar_token_declaration)
